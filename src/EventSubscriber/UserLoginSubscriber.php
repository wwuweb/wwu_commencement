<?php

namespace Drupal\wwu_commencement\EventSubscriber;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Url;
use Drupal\cas\Event\CasPostLoginEvent;
use Drupal\cas\Service\CasHelper;
use Drupal\wwu_commencement\Services\Settings\SettingsService;
use Drupal\wwu_commencement\Services\UserReservationService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Subscribes to the UserLoginEvent.
 *
 * @package Drupal\wwu_commencement\EventSubscriber
 */
final class UserLoginSubscriber implements EventSubscriberInterface {

  /**
   * Logger
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  private $logger;

  /**
   * Request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  private $requestStack;

  /**
   * User reservation service.
   *
   * @var \Drupal\wwu_commencement\Services\UserReservationService
   */
  private $userReservationService;

  /**
   * Settings service.
   *
   * @var \Drupal\wwu_commencement\Services\Settings\SettingsService
   */
  private $settings;

  /**
   * Constructs a UserLoginSubscriber object.
   *
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The requests stack.
   * @param \Drupal\wwu_commencement\Services\UserReservationService $user_reservation_service
   *   The submission service.
   * @param \Drupal\wwu_commencement\Services\Settings\SettingsService $settings_service
   *   The settings type service.
   */
  public function __construct(LoggerChannelFactoryInterface $logger_factory, RequestStack $request_stack, UserReservationService $user_reservation_service, SettingsService $settings_service) {
    $this->logger = $logger_factory->get('wwu_commencement');
    $this->requestStack = $request_stack;
    $this->userReservationService = $user_reservation_service;
    $this->settings = $settings_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      CasHelper::EVENT_POST_LOGIN => 'onUserLogin',
    ];
  }

  /**
   * Subscibe to the user login event.
   *
   * @param \Drupal\cas\Event\CasPostLoginEvent $event
   *   The CAS post login event.
   */
  public function onUserLogin(CasPostLoginEvent $event) {
    $account = $event->getAccount();
    $account_name = $account->getAccountName();
    $node_type = $this->settings->get('reservation_node_type');
    $record = $this->userReservationService->fetch($node_type);
    if ($record) {
      $this->logger->notice('Redirecting to existing submission for %account-name.', ['%account-name' => $account_name]);
    }
    else {
      $this->logger->notice('Redirecting to new submission for %account-name.', ['%account-name' => $account_name]);
    }
    $url = Url::fromRoute('wwu_commencement.reservation')->toString(TRUE)->getGeneratedUrl();
    $this->requestStack->getCurrentRequest()->query->set('destination', $url);
  }

}
