<?php

namespace Drupal\wwu_commencement\Services;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\wwu_commencement\Services\Settings\SettingsService;

/**
 * Provides email functionality to the reservation system.
 */
final class MailService {
  use StringTranslationTrait;

  /**
   * Current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Mail manager.
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  private $mailManager;

  /**
   * Date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  private $dateFormatter;

  /**
   * Settings service.
   *
   * @var \Drupal\wwu_commencement\Services\Settings\SettingsService
   */
  private $settings;

  /**
   * Constructs a MailService object.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Mail\MailManagerInterface $mail_manager
   *   The core mail manager.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter.
   * @param \Drupal\wwu_commencement\Services\Settings\SettingsService $settings_service
   *   The settings service.
   */
  public function __construct(AccountProxyInterface $current_user, MailManagerInterface $mail_manager, DateFormatterInterface $date_formatter, SettingsService $settings_service) {
    $this->currentUser = $current_user;
    $this->mailManager = $mail_manager;
    $this->dateFormatter = $date_formatter;
    $this->settings = $settings_service;
  }

  /**
   * Send the reservation confirmation email.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The reservation node object.
   */
  public function send(EntityInterface $entity) {
    $module = 'wwu_commencement';
    $key = 'commencement_confirmation';
    $to = $this->currentUser->getEmail();
    $langcode = $this->currentUser->getPreferredLangcode();
    $subject = $this->subject();
    $body = $this->body($entity);
    $params = ['subject' => $subject, 'body' => $body];
    $reply = NULL;
    $send = TRUE;
    $this->mailManager->mail($module, $key, $to, $langcode, $params, $reply, $send);
    foreach ($entity->field_confirmation_email->getIterator() as $email) {
      $this->mailManager->mail($module, $key . '_secondary', $email->value, $langcode, $params, $reply, $send);
    }
  }

  /**
   * The subject line for the confirmation email.
   *
   * @return string
   */
  private function subject() {
    $commencement_date = $this->settings->get('commencement_date');
    return $this->t('Confirming Your @commencement_date Commencement Reservation', ['@commencement_date' => $commencement_date]);
  }

  /**
   * The message body for the confirmation email.
   *
   * @return string
   */
  private function body(EntityInterface $entity) {
    $commencement_date = $this->settings->get('commencement_date');
    if ($entity->field_attending_commencement->value == 1) {
      $ceremony_timestamp = $entity->field_ceremony->date->getTimestamp();
      $ceremony_date = $this->dateFormatter->format($ceremony_timestamp, 'custom', 'l, F j, Y');
      $ceremony_time = $this->dateFormatter->format($ceremony_timestamp, 'custom', 'g:i A');
      $body =
        "Your reservation for @commencement_date Commencement has been recieved."
        . " "
        . "Please plan to attend your selected ceremony on @ceremony_date at @ceremony_time."
        . " "
        . "You can return to the reservation form anytime before November 15, 2024 to make edits."
        . "\r\n"
        . "\r\n"
        . "Ceremony participants must arrive 45 minutes in advance of their ceremony time for line up in Red Square."
        . "\r\n"
        . "\r\n"
        . "Guests will experience open seating for this event."
        . " "
        . "Tickets will not be issued."
        . " "
        . "We requested your number of guests on your reservation in order to plan for appropriate amounts of parking lot shuttles, anticipate if overflow seating is necessary, place orders for Commencement Programs, and share anticipated visitor volumes with our campus partners who staff complementary services."
        . "\r\n"
        . "\r\n"
        . "All ceremonies will be held in Carver Gym."
        . " "
        . "Doors will be unlocked 1 hour prior to the ceremony start time for guests to self-select their seats."
        . " "
        . "We ask that all guests are seated at least 15 minutes prior to ceremony start time."
        . " "
        . "Guests arriving later than 15 minutes prior may be asked to wait outside of Carver while the graduates process into the building."
        . "\r\n"
        . "\r\n"
        . "The commencement website will continue to be updated as details become available for participants and guests at https://registrar.wwu.edu/june-commencement."
        . "\r\n"
        . "\r\n"
        . "Contact the Commencement Team at commencement@wwu.edu with any questions."
        . "\r\n";
      $args = [
        '@commencement_date' => $commencement_date,
        '@ceremony_date' => $ceremony_date,
        '@ceremony_time' => $ceremony_time,
      ];
    }
    else {
      $body =
        "Thank you for confirming that you will NOT be attending the @commencement_date Commencement ceremony."
        . " "
        . "If your plans change and you are able to participate, you can return to https://commencement.wwu.edu/reservation at any time before November 15, 2024 to update your reservation."
        . "\r\n"
        . "\r\n"
        . "Contact the Commencement Team at commencement@wwu.edu with any questions."
        . "\r\n";
      $args = [
        '@commencement_date' => $commencement_date,
      ];
    }
    return $this->t($body, $args);
  }

}
