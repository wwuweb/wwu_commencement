<?php

namespace Drupal\wwu_commencement\Services\Settings;

use Drupal\Core\Config\Config;
use Drupal\Core\Form\FormStateInterface;

final class ReservationNodeTypeSetting extends EntitySettingBase {

  /**
   * {@inheritdoc}
   */
  public function getSettingKey() {
    return 'reservation_node_type';
  }

  /**
   * {@inheritdoc}
   */
  public function get(Config $config) {
    $id = parent::get($config);
    return $id ? $this->entityLoad('node_type', $id) : $this->entityLoad('node_type', 'graduation_reservation');
  }

  /**
   * {@inheritdoc}
   */
  public function buildFormElement(array $form, FormStateInterface $form_state, Config $config) {
    return [
      '#type' => 'entity_autocomplete',
      '#target_type' => 'node_type',
      '#title' => $this->t('Graduation Reservation Content Type'),
      '#description' => $this->t('Select the content type for Graduation Reservations.'),
      '#default_value' => $this->get($config),
      '#tags' => TRUE,
    ];
  }

}
