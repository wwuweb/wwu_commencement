<?php

namespace Drupal\wwu_commencement\Services\Settings;

use Drupal\Core\Config\Config;
use Drupal\Core\Form\FormStateInterface;

final class CommencementDateSetting extends SettingBase {

  /**
   * {@inheritdoc}
   */
  public function getSettingKey() {
    return 'commencement_date';
  }

  /**
   * {@inheritdoc}
   */
  public function buildFormElement(array $form, FormStateInterface $form_state, Config $config) {
    return [
      '#type' => 'textfield',
      '#title' => $this->t('Commencement Date'),
      '#description' => $this->t('The month and year for the upcoming commencement ceremony.'),
      '#default_value' => $this->get($config),
    ];
  }

}
