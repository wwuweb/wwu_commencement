<?php

namespace Drupal\wwu_commencement\Services\Settings;

use Drupal\Core\Config\Config;
use Drupal\Core\Form\FormStateInterface;

interface SettingInterface {

  /**
   * Get the setting key for this provider.
   *
   * @return string
   *   The setting key.
   */
  public function getSettingKey();

  /**
   * Provision the setting controlled by this provider.
   *
   * @param \Drupal\Core\Config\Config $config
   *   The config to set.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state to read.
   */
  public function set(Config $config, FormStateInterface $form_state);

  /**
   * Retrieve the setting controlled by this provider.
   *
   * @param \Drupal\Core\Config\Config $config
   *   The config.
   */
  public function get(Config $config);

  /**
   * Build the form element for this setting.
   *
   * @param array $form
   *   The form to build from.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state to read.
   * @param \Drupal\Core\Config\Config $config
   *   The config to set.
   *
   * @return array
   *   The form element.
   */
  public function buildFormElement(array $form, FormStateInterface $form_state, Config $config);

}
