<?php

namespace Drupal\wwu_commencement\Services\Settings;

use Drupal\Core\Config\Config;
use Drupal\Core\Form\FormStateInterface;

final class SuccessPageSetting extends EntitySettingBase {

  /**
   * {@inheritdoc}
   */
  public function getSettingKey() {
    return 'success_page';
  }

  /**
   * {@inheritdoc}
   */
  public function get(Config $config) {
    $id = parent::get($config);
    return $id ? $this->entityLoad('node', $id) : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function buildFormElement(array $form, FormStateInterface $form_state, Config $config) {
    return [
      '#type' => 'entity_autocomplete',
      '#target_type' => 'node',
      '#title' => $this->t('Success Page'),
      '#description' => $this->t('Select the reservation success confirmation page.'),
      '#default_value' => $this->get($config),
      '#tags' => TRUE,
      '#selection_settings' => [
        'target_bundles' => ['page']
      ],
    ];
  }

}
