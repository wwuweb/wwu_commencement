<?php

namespace Drupal\wwu_commencement\Services\Settings;

use Drupal\Core\Config\Config;
use Drupal\Core\Form\FormStateInterface;

final class ReservationRoleSetting extends EntitySettingBase {

  /**
   * {@inheritdoc}
   */
  public function getSettingKey() {
    return 'reservation_role';
  }

  /**
   * {@inheritdoc}
   */
  public function get(Config $config) {
    $id = parent::get($config);
    return $id ? $this->entityLoad('user_role', $id) : $this->entityLoad('user_role', 'graduate');
  }

  /**
   * {@inheritdoc}
   */
  public function buildFormElement(array $form, FormStateInterface $form_state, Config $config) {
    return [
      '#type' => 'entity_autocomplete',
      '#target_type' => 'user_role',
      '#title' => $this->t('Graduation Reservation User Role'),
      '#description' => $this->t('Select the role for Graduation Reservation users.'),
      '#default_value' => $this->get($config),
      '#tags' => TRUE,
    ];
  }

}
