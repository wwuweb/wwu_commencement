<?php

namespace Drupal\wwu_commencement\Services\Settings;

use Drupal\Core\Config\Config;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

abstract class SettingBase implements SettingInterface {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  abstract public function getSettingKey();

  /**
   * {@inheritdoc}
   */
  public function set(Config $config, FormStateInterface $form_state) {
    $key = $this->getSettingKey();
    $config->set($key, $form_state->getValue($key));
  }

  /**
   * {@inheritdoc}
   */
  public function get(Config $config) {
    return $config->get($this->getSettingKey());
  }

  /**
   * {@inheritdoc}
   */
  abstract public function buildFormElement(array $form, FormStateInterface $form_state, Config $config);

}

