<?php

namespace Drupal\wwu_commencement\Services\Settings;

use Drupal\Core\Config\Config;
use Drupal\Core\Form\FormStateInterface;

final class ReservationsEnabledSetting extends SettingBase {

  /**
   * {@inheritdoc}
   */
  public function getSettingKey() {
    return 'reservations_enabled';
  }

  /**
   * {@inheritdoc}
   */
  public function buildFormElement(array $form, FormStateInterface $form_state, Config $config) {
    return [
      '#type' => 'checkbox',
      '#title' => $this->t('Reservations Enabled'),
      '#default_value' => $this->get($config),
    ];
  }

}
