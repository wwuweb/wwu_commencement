<?php

namespace Drupal\wwu_commencement\Services\Settings;

use Drupal\Core\Config\Config;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;

abstract class EntitySettingBase extends SettingBase {

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Default constructor for a EntitySetting object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function set(Config $config, FormStateInterface $form_state) {
    $key = $this->getSettingKey();
    $value = $form_state->getValue($key);
    if (empty($value)) {
      $config->set($key, NULL);
    }
    else {
      $config->set($key, $value[0]['target_id']);
    }
  }

  /**
   * Load an entity of the given type.
   *
   * @param string $entity_type
   *   The name of the entity type to load.
   * @param string $id
   *   The id of the entity to load.
   */
  protected function entityLoad($entity_type, $id) {
    return $this->entityTypeManager->getStorage($entity_type)->load($id);
  }

}


