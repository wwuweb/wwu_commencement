<?php

namespace Drupal\wwu_commencement\Services\Settings;

use Drupal\Core\Config\Config;
use Drupal\Core\Form\FormStateInterface;

final class CeremonyDateFormatSetting extends EntitySettingBase {

  /**
   * {@inheritdoc}
   */
  public function getSettingKey() {
    return 'ceremony_date_format';
  }

  /**
   * {@inheritdoc}
   */
  public function get(Config $config) {
    $id = parent::get($config);
    return $id ? $this->entityLoad('date_format', $id) : $this->entityLoad('date_format', 'ceremony_date');
  }

  /**
   * {@inheritdoc}
   */
  public function buildFormElement(array $form, FormStateInterface $form_state, Config $config) {
    return [
      '#type' => 'entity_autocomplete',
      '#target_type' => 'date_format',
      '#title' => $this->t('Ceremony Date Format'),
      '#description' => $this->t('Select the date format to use for ceremony dates and times.'),
      '#default_value' => $this->get($config),
      '#tags' => TRUE,
    ];
  }

}
