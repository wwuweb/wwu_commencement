<?php

namespace Drupal\wwu_commencement\Services\Settings;

use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Service for managing module settings.
 */
final class SettingsService {

  /**
   * Configuration name.
   *
   * @var string
   */
  private $configName;

  /**
   * Configuration object.
   *
   * @var \Drupal\Core\Config\Config
   */
  private $config;

  /**
   * Setting providers.
   *
   * @var array
   */
  private $providers = [];

  /**
   * Constructs a new SettingsService object.
   *
   * @param string $config_name
   *   The name of the configuration to use.
   * @param \Drupal\Core\Config\ConfigFactory\Interface $config_factory
   *   The config factory.
   */
  public function __construct(string $config_name, ConfigFactoryInterface $config_factory) {
    $this->configName = $config_name;
    $this->config = $config_factory->get($config_name);
  }

  /**
   * Get the name of this config.
   *
   * @return string
   *   The name of the config.
   */
  public function getConfigName() {
    return $this->configName;
  }

  /**
   * Service collector callback for adding a provider.
   *
   * @param \Drupal\wwu_commencement\Services\SettingInterface $provider
   *   The provider to collect.
   */
  public function addProvider(SettingInterface $provider) {
    $this->providers[$provider->getSettingKey()] = $provider;
  }

  /**
   * Build the settings form.
   *
   * @param array $form
   *   The form to build from.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state to read.
   *
   * @return array
   *   The form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    foreach ($this->providers as $provider) {
      $form[$provider->getSettingKey()] = $provider->buildFormElement($form, $form_state, $this->config);
    }
    return $form;
  }

  /**
   * Update all settings from config form submission.
   *
   * @param \Drupal\Core\Config\Config $config
   *   The config to set.
   * @param array $form
   *   The form to build from.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state to read.
   */
  public function submitForm(Config $config, array &$form, FormStateInterface $form_state) {
    foreach ($this->providers as $provider) {
      $provider->set($config, $form_state);
    }
  }

  /**
   * Get the given setting value.
   *
   * @param string $key
   *   The setting key to get. This must match the setting key corresponding to
   *   a setting provider.
   *
   * @return string
   *   The value of the setting.
   */
  public function get(string $key) {
    $provider = $this->providers[$key];
    return $provider->get($this->config);
  }

}
