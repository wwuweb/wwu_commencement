<?php

namespace Drupal\wwu_commencement\Services;

use Drupal\Core\Database\Connection;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\node\NodeTypeInterface;

/**
 * Fetches the first instance of the configured reservation content created by
 * the current user.
 */
final class UserReservationService {

  /**
   * Database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  private $database;

  /**
   * Current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  private $currentUser;

  /**
   * Constructs a UserReservationService object.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user.
   */
  public function __construct(Connection $database, AccountProxyInterface $current_user) {
    $this->database = $database;
    $this->currentUser = $current_user;
  }

  /**
   * Fetch the record for the current user.
   *
   * @param \Drupal\node\NodeTypeInterface $node_type
   *   The node type to query for entries.
   *
   * @return
   *   Result row.
   */
  public function fetch(NodeTypeInterface $node_type) {
    $sql = "SELECT nid FROM {node_field_data} WHERE uid = :uid AND type = :type";
    $result =  $this->database->query($sql, [
      ':uid' => $this->currentUser->id(),
      ':type' => $node_type->id(),
    ]);
    return $result->fetch();
  }

}
