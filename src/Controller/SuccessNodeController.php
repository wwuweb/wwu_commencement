<?php

namespace Drupal\wwu_commencement\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Provides a custom controller to deny access to the reservation submission
 * success page node, so that it can only be viewed in situ.
 */
final class SuccessNodeController extends ControllerBase {

  /**
   * Access callback for node view route.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user account.
   * @param \Drupal\Core\Entity\EntityInterface $node
   *   The node.
   */
  public function access(AccountInterface $account, EntityInterface $node) {
    $success_page = $this->config('wwu_commencement.settings')->get('success_page');
    if ($node->id() === $success_page) {
      return AccessResult::allowedIf($account->hasPermission('administer content'));
    }
    else {
      return AccessResult::allowed();
    }
  }

}
