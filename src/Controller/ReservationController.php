<?php

namespace Drupal\wwu_commencement\Controller;

use Drupal\Component\Uuid\Uuid;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\Core\TempStore\TempStoreExcpetion;
use Drupal\wwu_commencement\Exception\MissingUserFieldsException;
use Drupal\wwu_commencement\Services\Settings\SettingsService;
use Drupal\wwu_commencement\Services\UserReservationService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Controller for handling reservation form routes.
 */
final class ReservationController extends ControllerBase {

  /**
   * Node type service.
   *
   * @var \Drupal\wwu_commencement\Serbices\Settings\SettingsService
   */
  private $settings;

  /**
   * User reservation service.
   *
   * @var \Drupal\wwu_commencement\Services\UserReservationService
   */
  private $userReservationService;

  /**
   * Request stack.
   *
   * @var \Symfony\Componet\HttpFoundation\RequestStack
   */
  private $requestStack;

  /**
   * Temp store factory.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  private $tempStoreFactory;

  /**
   * Constructs a Reservation Controller object.
   *
   * @param \Drupal\wwu_commencement\Services\UserReservationService $user_reservation_service
   *   The user reservation service.
   * @param \Drupal\wwu_commencement\Services\Settings\SettingsService $settings_service
   *   The settings service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store_factory
   *   The temp store factory.
   */
  public function __construct(SettingsService $settings_service, UserReservationService $user_reservation_service, RequestStack $request_stack, PrivateTempStoreFactory $temp_store_factory) {
    $this->settings = $settings_service;
    $this->userReservationService = $user_reservation_service;
    $this->requestStack = $request_stack;
    $this->tempStoreFactory = $temp_store_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container
      ->get('wwu_commencement.settings_service'), $container
      ->get('wwu_commencement.user_reservation_service'), $container
      ->get('request_stack'), $container
      ->get('tempstore.private'));
  }

  /**
   * Callback for reservation route.
   *
   * Retrieve an existing reservation if it exists and provide the edit form,
   * or otherwise provide the create form.
   *
   * @return array
   *   The reservation form render array.
   */
  public function reservation() {
    try {
      $store = $this->tempStoreFactory->get('wwu_commencement');
      $store->delete('entity');
    }
    catch (TempStoreException $e) {
      $this->getLogger('wwu_commencement')->error($e->getMessage());
    }
    try {
      $node_type = $this->settings->get('reservation_node_type');
      $record = $this->userReservationService->fetch($node_type);
      if (empty($record)) {
        $node = $this->entityTypeManager()->getStorage('node')->create(['type' => $node_type->id()]);
      }
      else {
        $node = $this->entityTypeManager()->getStorage('node')->load($record->nid);
      }
      return $this->entityFormBuilder()->getForm($node, 'commencement');
    }
    catch (EntityStorageException | MissingUserFieldsException $e) {
      $this->getLogger('wwu_commencement')->error($e->getMessage());
      $this->messenger()->addError('There was a problem loading the reservation form.');
      return [
        '#type' => 'markup',
        '#markup' => $this->t('<p>Unfortunately, we ran into an error with the reservation process. Please contact <a href="@webhelp-address">webhelp@wwu.edu</a> for further assistance.</p>', [
          '@webhelp-address' => 'mailto:webhelp@wwu.edu',
        ]),
      ];
    }
  }

  /**
   * Title callback form reservation form page.
   */
  public function reservationTitle() {
    return $this->t('Commencement Reservations');
  }

  /**
   * Access callback for reservation form page.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user account.
   * @return \Drupal\Core\Access\AccessResult
   *   The access result.
   */
  public function reservationAccess(AccountInterface $account) {
    $reservations_enabled = $this->settings->get('reservations_enabled');
    return AccessResult::allowedIf($reservations_enabled && $account->hasPermission('access reservation form'));
  }

  /**
   * Callback for success route.
   *
   * Provide the submission success page.
   *
   * @return array
   *   Success page render array.
   */
  public function success() {
    // Check the temporary flag to determine if the user has submitted the form
    // succesfully. If not, we will redirect the user to the form.
    $store = $this->tempStoreFactory->get('wwu_commencement');
    if ($store->get('success')) {
      $store->delete('success');
      $this->messenger()->addStatus('Your commencement reservation has been received.');
      $success_page = $this->settings->get('success_page');
      if (empty($success_page)) {
        return $this->redirect('<front>');
      }
      else {
        return $this->entityTypeManager()->getViewBuilder('node')->view($success_page);
      }
    }
    else {
      return $this->redirect('wwu_commencement.reservation');
    }
  }

  /**
   * Title callback for success route.
   *
   * @return string
   *   Success page title.
   */
  public function successTitle() {
    $success_page = $this->settings->get('success_page');
    if (empty($success_page)) {
      return $this->t('Commencement Reservation Confirmed');
    }
    else {
      return $success_page->getTitle();
    }
  }

  /**
   * Access callback for success form page.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user account.
   * @return \Drupal\Core\Access\AccessResult
   *   The access result.
   */
  public function successAccess(AccountInterface $account) {
    $reservations_enabled = $this->settings->get('reservations_enabled');
    return AccessResult::allowedIf($reservations_enabled && $account->hasPermission('access reservation form'));
  }

}
