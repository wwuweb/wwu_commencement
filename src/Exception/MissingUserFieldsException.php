<?php

namespace Drupal\wwu_commencement\Exception;

class MissingUserFieldsException extends \Exception { }
