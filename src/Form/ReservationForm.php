<?php

namespace Drupal\wwu_commencement\Form;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\node\NodeForm;
use Drupal\wwu_commencement\Services\MailService;
use Drupal\wwu_commencement\Services\Settings\SettingsService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * This overrides the default Node From to insert a confirmation form after
 * saving the node, as well as provide other customizations.
 */
final class ReservationForm extends NodeForm {

  /**
   * Settings service.
   *
   * @var \Drupal\wwu_commencement\Services\Settings\SettingsService
   */
  protected $settings;

  /**
   * Mail service.
   *
   * @var \Drupal\wwu_commencement\Services\MailService
   */
  protected $mailService;

  /**
   * Date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The current user account.
   *
   * @var \Drupal\user\Entity\UserInterface
   */
  protected $account;

  /**
   * Set the settings service.
   *
   * @param \Drupal\wwu_commencement\Services\Settings\SettingsService $settings_service
   *   The settings service.
   */
  public function setSettingsService(SettingsService $settings_service) {
    $this->settings = $settings_service;
    return $this;
  }

  /**
   * Set the mail service.
   *
   * @param \Drupal\wwu_commencement\Services\MailService $mail_service
   *   The mail service.
   */
  public function setMailService(MailService $mail_service) {
    $this->mailService = $mail_service;
    return $this;
  }

  /**
   * Set the date formatter.
   *
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter.
   */
  public function setDateFormatter(DateFormatterInterface $date_formatter) {
    $this->dateFormatter = $date_formatter;
    return $this;
  }

  /**
   * Set the current user account.
   *
   * Must be populated by calling the setAccount() method (e.g. in create())
   * before calling any creational methods to construct the form.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   */
  public function setAccount(EntityTypeManagerInterface $entity_type_manager, AccountInterface $current_user) {
    $this->account = $entity_type_manager->getStorage('user')->load($current_user->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->setSettingsService($container->get('wwu_commencement.settings_service'));
    $instance->setMailService($container->get('wwu_commencement.mail_service'));
    $instance->setDateFormatter($container->get('date.formatter'));
    $instance->setAccount($container->get('entity_type.manager'), $container->get('current_user'));
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    // Get the base node form from the parent form class, which in this case is
    // the default Node form.
    $form = parent::form($form, $form_state);

    // Add the node type help text to the form as a markup item.
    $node_type = $this->entityTypeManager->getStorage('node_type')->load($this->entity->getType());
    $form['help'] = [
      '#type' => 'item',
      '#markup' => $node_type->getHelp(),
      '#weight' => 0,
    ];

    // Add the commencement reservation confirmation fields to the form.
    $commencement_date = $this->settings->get('commencement_date');
    $form['attending_commencement'] = [
      '#type' => 'radios',
      '#title' => $this->t('Confirm Attendance'),
      '#description' => $this->t('Will you be attending @commencement-date Commencement?', [
        '@commencement-date' => $commencement_date,
      ]),
      '#description_display' => 'before',
      '#required' => TRUE,
      '#weight' => 1,
      '#default_value' => ((int) $this->entity->get('field_attending_commencement')->getString()) === 0 ? 1 : 0,
      '#options' => [
        0 => $this->t('Yes, I will be attending @commencement-date Commencement.', [
          '@commencement-date' => $commencement_date,
        ]),
        1 => $this->t('No, I will not be attending @commencement-date Commencement.', [
          '@commencement-date' => $commencement_date,
        ]),
      ],
    ];

    // Set the value of the corrected diploma name field.
    $diploma_name = $this->account->field_corrected_diploma_name->value;
    $form['field_diploma_name']['widget'][0]['value']['#default_value'] = $diploma_name;
    $form['field_diploma_name']['widget'][0]['value']['#value'] = $diploma_name;
    $form['field_diploma_name']['widget'][0]['value']['#attributes']['readonly'] = 'readonly';
    $form['field_diploma_name']['widget'][0]['value']['#attributes']['disabled'] = 'disabled';

    // Alter the home town field display.
    $form['field_home_town']['widget'][0]['#type'] = 'fieldset';
    $form['field_home_town']['widget'][0]['#description_display'] = 'before';
    $form['field_home_town']['widget'][0]['#collapsible'] = FALSE;
    $form['field_home_town']['widget'][0]['#collapsed'] = FALSE;

    // Add the reservation fieldset.
    $form['reservation'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Make a reservation for Commencement'),
      '#description_display' => 'before',
      '#weight' => 2,
      '#states' => [
        'visible' => [':input[name="attending_commencement"]' => ['value' => 0]],
      ],
    ];

    // Move the related fields to the reservation fieldset.
    $this->moveFieldToFieldset($form, 'reservation', 'field_diploma_name');
    $this->moveFieldToFieldset($form, 'reservation', 'field_phonetic_spelling');
    $this->moveFieldToFieldset($form, 'reservation', 'field_name_pronunciation');
    $this->moveFieldToFieldset($form, 'reservation', 'field_number_of_guests');

    // Populate the ceremonies proxy field options array.
    $ceremony_options = [];
    foreach ($this->account->field_ceremonies->getIterator() as $delta => $ceremony) {
      $ceremony_timestamp = $ceremony->date->getTimestamp();
      $ceremony_label = $this->dateFormatter->format($ceremony_timestamp, 'ceremony_date');
      $ceremony_value = $this->dateFormatter->format($ceremony_timestamp, 'custom', DateTimeItemInterface::DATETIME_STORAGE_FORMAT, DateTimeItemInterface::STORAGE_TIMEZONE);
      $ceremony_code = $this->account->field_ceremony_codes->get($delta)->value;
      $ceremony_options[$ceremony_value] = $ceremony_code . ": " . $ceremony_label;
    }

    // Get the currently selected ceremony, if any.
    if ($selected_ceremony_date = $this->entity->field_ceremony->date) {
      $default_ceremony = $this->dateFormatter->format($selected_ceremony_date->getTimestamp(), 'custom', DateTimeItemInterface::DATETIME_STORAGE_FORMAT, DateTimeItemInterface::STORAGE_TIMEZONE);
    }

    // Add a proxy field to present available ceremony options, and set the
    // options array to the calculated value.
    $form['reservation']['ceremonies'] = [
      '#type' => 'select',
      '#title' => $this->t('Ceremony'),
      '#description' => '<p>' . $this->t('Select the ceremony you plan to attend based on the program(s) you are enrolled in. Please refer to the <a href="https://registrar.wwu.edu/december-commencement-schedule-and-program">commencement schedule</a> when making your selection.') . '</p>',
      '#weight' => $form['reservation']['field_number_of_guests']['#weight'] + 1,
      '#options' => $ceremony_options,
      '#default_value' => $default_ceremony ?? '',
      '#empty_option' => '- Select -',
      '#states' => [
        'required' => [':input[name="attending_commencement"]' => ['value' => 0]],
      ],
    ];

    // Add the graduation slide fieldset.
    $form['graduation_slide'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Graduate Slide'),
      '#description' => '<p>' . $this->t('Providing any of the following information is <strong>optional</strong>. If submitted, it will appear on your Graduation Slide, to be published on the Western Commencement website.') . '</p>'. '<p>' . $this->t('If you choose not to "publish" your slide, you will not be listed and will be unsearchable in association with your ceremony on the Commencement website.') . '</p>',
      '#description_display' => 'before',
      '#weight' => 4,
      '#states' => [
        'visible' => [':input[name="attending_commencement"]' => ['value' => 0]],
      ],
    ];

    // Move the related fields to the graduation slide fieldset.
    $this->moveFieldToFieldset($form, 'graduation_slide', 'field_home_town');
    $this->moveFieldToFieldset($form, 'graduation_slide', 'field_graduation_photo');
    $this->moveFieldToFieldset($form, 'graduation_slide', 'field_image_rotation');
    $this->moveFieldToFieldset($form, 'graduation_slide', 'field_quote');

    // Populate the college proxy field options array.
    $college_options = [];
    $college_options[] = trim($this->account->field_coll_1->value ?? '');
    $college_options[] = trim($this->account->field_coll_2->value ?? '');
    $college_value = implode(', ', array_filter($college_options));

    // Add a proxy field to present available college options.
    $form['graduation_slide']['college'] = [
      '#type' => 'textfield',
      '#title' => $this->t('College'),
      '#weight' => $form['graduation_slide']['field_home_town']['#weight'] - 4,
      '#value' => $college_value,
      '#default_value' => $college_value,
      '#attributes' => [
        'readonly' => 'readonly',
        'disabled' => 'disabled',
      ],
    ];

    // Populate the degree type field options array.
    $degree_type_options = [];
    $degree_type_options[] = trim($this->account->field_degc_1->value ?? '');
    $degree_type_options[] = trim($this->account->field_degc_2->value ?? '');
    $degree_type_value = implode(', ', array_filter($degree_type_options));

    // Add a proxy field to present available degree type options.
    $form['graduation_slide']['degree_type'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Degree Type'),
      '#weight' => $form['graduation_slide']['field_home_town']['#weight'] - 3,
      '#value' => $degree_type_value,
      '#default_value' => $degree_type_value,
      '#attributes' => [
        'readonly' => 'readonly',
        'disabled' => 'disabled',
      ],
    ];

    // Populate the major proxy field options array.
    $major_options = [];
    $major_options[] = trim($this->account->field_major_desc_1->value ?? '');
    $major_options[] = trim($this->account->field_major_desc_2->value ?? '');
    $major_value = implode("\n", array_filter($major_options));

    // Add a proxy field to present available major options.
    $form['graduation_slide']['major'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Major'),
      '#weight' => $form['graduation_slide']['field_home_town']['#weight'] - 2,
      '#value' => $major_value,
      '#default_value' => $major_value,
      '#rows' => 2,
      '#attributes' => [
        'readonly' => 'readonly',
        'disabled' => 'disabled',
      ],
    ];
    
    // Get the graduation quarter value from the user profile.
    $grad_term_value = $this->account->field_grad_term->value;

    // Add a proxy field to present the graduation quarter.
    $form['graduation_slide']['grad_term'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Graduation Quarter'),
      '#weight' => $form['graduation_slide']['field_home_town']['#weight'] - 1,
      '#value' => $grad_term_value,
      '#default_value' => $grad_term_value,
      '#rows' => 2,
      '#attributes' => [
        'readonly' => 'readonly',
        'disabled' => 'disabled',
      ],
    ];

    // Add the conformation email fieldset.
    $form['confirmation_email'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Reservation Notification'),
      '#description' => '<p>' . $this->t('<strong>Optional:</strong> provide additional email addresses to receive confirmation of your commencement reservation. <strong>You will always receive a confirmation in your Western inbox.</strong>') . '</p>',
      '#description_display' => 'before',
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
      '#tree' => FALSE,
      '#weight' => 5,
      '#states' => [
        'visible' => [':input[name="attending_commencement"]' => ['value' => 0]],
      ],
    ];

    // Move the related fields to the confirmation email fieldset.
    $this->moveFieldToFieldset($form, 'confirmation_email', 'field_confirmation_email');

    // Add the information consent fieldset.
    $form['information_consent_wrapper'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Disclosure Acknowledgement'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
      '#tree' => FALSE,
      '#weight' => 6,
      '#states' => [
        'visible' => [':input[name="attending_commencement"]' => ['value' => 0]],
      ],
    ];

    // Add the information consent element.
    $form['information_consent_wrapper']['information_consent'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('By checking this box, I grant Western Washington University permission to publish on its website a graduation slide that includes the information provided with this form (e.g. photo, hometown, message of thanks) as well as my degree, major, and honors. If you choose not to "publish" your slide, you will not be listed and will be unserachable in association with your ceremony on the Commencement website.'),
    ];

    // Hide fields that should not be displayed to the end user.
    $this->hideInternalField($form, 'meta');
    $this->hideInternalField($form, 'title');
    $this->hideInternalField($form, 'field_academic_honors');
    $this->hideInternalField($form, 'field_attending_commencement');
    $this->hideInternalField($form, 'field_ceremony');
    $this->hideInternalField($form, 'field_ceremony_code');
    $this->hideInternalField($form, 'field_degree_1');
    $this->hideInternalField($form, 'field_degree_1_major_1');
    $this->hideInternalField($form, 'field_degree_1_major_2');
    $this->hideInternalField($form, 'field_degree_2');
    $this->hideInternalField($form, 'field_degree_2_major_1');
    $this->hideInternalField($form, 'field_first_name');
    $this->hideInternalField($form, 'field_information_consent');
    $this->hideInternalField($form, 'field_outstanding_graduate');
    $this->hideInternalField($form, 'field_presidential_scholar');
    $this->hideInternalField($form, 'field_selected_college');
    $this->hideInternalField($form, 'field_selected_degree_type');
    $this->hideInternalField($form, 'field_selected_major');
    $this->hideInternalField($form, 'field_university_honors');
    $this->hideInternalField($form, 'field_veteran');
    $this->hideInternalField($form, 'field_w_number');

    // Return the completed form array.
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $node = $this->entity;
    $this->setProxyFields($node, $form_state);
    $insert = $node->isNew();
    try {
      // Save the submisison.
      $node->save();
      // Store a temporary flag indicating the successful submission. This flag
      // is referenced in the ReservationController::success() method, which
      // uses this flag to route the user away from the success page if they
      // have not made a submission.
      $store = $this->tempStoreFactory->get('wwu_commencement');
      $store->set('success', TRUE);
      // Log the submission to the database.
      $this->logSubmission($node, $insert);
      // Send the confirmation email.
      $this->sendConfirmationEmail($node);
      // Redirect to the success page.
      $form_state->setRedirect('wwu_commencement.success');
    }
    catch (EntityStorageException $e) {
      $this->messenger()->addError($this->t('Unfortunately, there was a problem submitting the form and we did not receive your reservation. Please contact <a href="@webhelp-address">WebHelp</a> for assistance.', [
        '@webhelp-address' => 'mailto:webhelp@wwu.edu',
      ]));
      $this->logger('wwu_commencement')->error('Reservation form submission failed for %account-name with the error %error.', [
        '%account-name' => $this->account->getAccountName(),
        '%error' => $e->getMessage(),
      ]);
      $form_state->setRebuild();
      $form_state->setRedirect('wwu_commencement.reservation');
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $element = parent::actions($form, $form_state);
    $element['submit']['#value'] = $this->t('Confirm & Submit');
    $element['delete']['#access'] = FALSE;
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  protected function addRevisionableFormFields(array &$form) {
    parent::addRevisionableFormFields($form);
    $form['revision_information']['#access'] = FALSE;
  }

  /**
   * Move the given field into the given fieldset.
   *
   * @param string $fieldset_name
   *   The name of the fieldset.
   * @param string $field_name
   *   The name of the field.
   */
  private function moveFieldToFieldset(array &$form, string $fieldset_name, string $field_name) {
    if (isset($form[$field_name])) {
      $form[$fieldset_name][$field_name] = $form[$field_name];
      unset($form[$field_name]);
    }
  }

  /**
   * Hide form fields that are set by the reservation system and should not be
   * altered by the user.
   *
   * These fields are populated automatically via LDAP query or Feeds import,
   * and so should not be seen or altered by the user. The separate 'display
   * name' field is used on the final slide and can be configured by the user.
   *
   * @param array $form
   *   The form.
   */
  private function hideInternalField(array &$form, string $field_name) {
    if (isset($form[$field_name])) {
      $form[$field_name]['#type'] = 'hidden';
      $form[$field_name]['#required'] = FALSE;
      $form[$field_name]['#access'] = FALSE;
    }
  }

  /**
   * Read proxy field values from the form and update the corresponding entity
   * fields on the node itself.
   *
   * "Proxy fields" are fields that only exist on the form as presented to the
   * user and are not directly stored in the database, unlike fields directly
   * attached to the node itself (e.g. fields added through
   * /admin/structure/types/manage/graduation_reservation/fields).
   *
   * Instead, proxy fields are defined in the form() method of this class and
   * therefore must be explicitily saved when the form is submitted.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The node to update.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The submitted form state.
   */
  private function setProxyFields(EntityInterface $entity, FormStateInterface $form_state) {
    // Set the attending commencement confirmation field.
    if ($entity->hasField('field_attending_commencement') && $form_state->hasValue('attending_commencement')) {
      if ((int) $form_state->getValue('attending_commencement') === 0) {
        // The first zero-indexed value of the attending commencement radio
        // select indicates an affirmative attendance.
        $entity->set('field_attending_commencement', 1);
      }
      else {
        // The second zero-indexed value of the attending commencement radio
        // select indicates a negative attendance.
        $entity->set('field_attending_commencement', 0);
      }
    }
    // Set the information consent confirmation field.
    if ($entity->hasField('field_information_consent') && $form_state->hasValue('information_consent')) {
      if ((int) $form_state->getValue('information_consent') === 1) {
        // A value of "1" indicates affirmative consent.
        $entity->set('field_information_consent', 1);
      }
      else {
        // Any other value indicataes negative consent.
        $entity->set('field_information_consent', 0);
      }
    }
    // Set the ceremony fields.
    if ($entity->hasField('field_ceremony') && $form_state->hasValue('ceremonies')) {
      $entity->set('field_ceremony', $form_state->getValue('ceremonies'));
      foreach ($this->account->field_ceremonies->getIterator() as $delta => $ceremony) {
        $ceremony_timestamp = $ceremony->date->getTimestamp();
        $ceremony_value = $this->dateFormatter->format($ceremony_timestamp, 'custom', DateTimeItemInterface::DATETIME_STORAGE_FORMAT, DateTimeItemInterface::STORAGE_TIMEZONE);
        if ($entity->get('field_ceremony')->value === $ceremony_value) {
          $entity->set('field_ceremony_code', $this->account->field_ceremony_codes->get($delta)->value);
        }
      }
    }
  }

  /**
   * Log the submission.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   * @param bool $insert
   *   Whether the entity is newly created or an update to an existing submission.
   */
  private function logSubmission(EntityInterface $entity, bool $insert) {
    $context = ['@type' => $entity->getType(), '%title' => $entity->label()];
    if ($insert) {
      $this->logger('content')->notice('@type: added %title.', $context);
    }
    else {
      $this->logger('content')->notice('@type: updated %title.', $context);
    }
  }

  /**
   * Send a confirmation email.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   */
  private function sendConfirmationEmail(EntityInterface $entity) {
    $result = $this->mailService->send($entity);
  }

}
