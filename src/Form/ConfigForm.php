<?php

namespace Drupal\wwu_commencement\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\wwu_commencement\Services\Settings\SettingsService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The admin configuration form class.
 */
final class ConfigForm extends ConfigFormBase {

  /**
   * The settings service.
   *
   * @var \Drupal\wwu_commencement\Services\Settings\SettingsService
   */
  private $settings;

  /**
   * Create a ConfigForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\wwu_commencement\Services\Settings\SettingsService $settings_service
   *   The settings service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, SettingsService $settings_service) {
    parent::__construct($config_factory);
    $this->settings = $settings_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container
      ->get('config.factory'), $container
      ->get('wwu_commencement.settings_service'));
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      $this->settings->getConfigName(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'wwu_commencement_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = $this->settings->buildForm($form, $form_state);
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $config = $this->config($this->settings->getConfigName());
    $this->settings->submitForm($config, $form, $form_state);
    $config->save();
  }

}
