<?php

namespace Drupal\wwu_commencement\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;
use Drupal\wwu_commencement\Exception\MissingUserFieldsException;

/**
 * Provides customizations for the bundle configured to handle graduation
 * reservations.
 */
final class Reservation extends Node {

  /**
   * {@inheritdoc}
   */
  public function postCreate(EntityStorageInterface $storage) {
    parent::postCreate($storage);
    $this->setInternalFields();
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);
    $this->setInternalFields();
  }

  /**
   * Auto-populate internal fields from the user object.
   *
   * We pull in W-Number and name from LDAP to auto-populate on the graduation
   * reservation node object.
   */
  private function setInternalFields() {
    if ($uid = $this->getOwnerId()) {
      $user = User::load($uid);
      if ($user->hasField('field_w_number') && ($w_number = $user->get('field_w_number')->value)) {
        $this->set('field_w_number', $w_number);
      }
      else {
        throw new MissingUserFieldsException('User object W number field was missing or empty.');
      }
      if ($user->hasField('field_first_name') && ($first_name = $user->get('field_first_name')->value)) {
        $this->set('field_first_name', $first_name);
      }
      else {
        throw new MissingUserFieldsException('User object first name field was missing or empty.');
      }
      if ($user->hasField('field_last_name') && ($last_name = $user->get('field_last_name')->value)) {
        $this->setTitle($last_name);
      }
      else {
        throw new MissingUserFieldsException('User object last name field was missing or empty');
      }
    }
  }

}
