<?php

namespace Drupal\wwu_commencement\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the MessageOfThanksElement constraint.
 */
final class MessageOfThanksElementValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate($items, Constraint $constraint) {
    foreach ($items as $item) {
      if (mb_strlen($item->value, 'UTF-8') > 150) {
        $this->context->addViolation($constraint->messageLength);
      }
    }
  }

}
