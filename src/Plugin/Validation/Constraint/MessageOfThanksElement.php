<?php

namespace Drupal\wwu_commencement\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Prevents a message of thanks from exceeding the diplay area of the graduate
 * slide.
 *
 * @Constraint(
 *   id = "MessageOfThanksElement",
 *   label = @Translation("Message of Thanks maximum length"),
 *   type = "text"
 * )
 */
final class MessageOfThanksElement extends Constraint {

  public $messageLength = 'Please enter a <em>Message of Thanks</em> under 150 characters in length.';

}
